
interface avatarType {
}

interface daily_bonusType {
   next_collected_timestamp_ms: number;
   next_defender_bonus_collect_timestamp_ms: number;
}

interface equipped_badgeType {
   badge_type: number;
   level: number;
   next_equip_change_allowed_timestamp_ms: number;
}

interface contact_settingsType {
   send_marketing_emails: boolean;
   send_push_notifications: boolean;
}

interface currenciesItemType {
   name: string;
   amount: number;
}

interface buddy_pokemonType {
   id: number;
   start_km_walked: number;
   last_km_awarded: number;
}

interface Player {
   creation_timestamp_ms: number;
   username: string;
   team: number;
   tutorial_state: Array<number>;
   avatar: avatarType;
   max_pokemon_storage: number;
   max_item_storage: number;
   daily_bonus: daily_bonusType;
   equipped_badge: equipped_badgeType;
   contact_settings: contact_settingsType;
   currencies: Array<currenciesItemType>;
   remaining_codename_claims: number;
   buddy_pokemon: buddy_pokemonType;
   level: number;
   experience: number;
   prev_level_xp: number;
   next_level_xp: number;
   km_walked: number;
   pokemons_encountered: number;
   unique_pokedex_entries: number;
   pokemons_captured: number;
   evolutions: number;
   poke_stop_visits: number;
   pokeballs_thrown: number;
   eggs_hatched: number;
   big_magikarp_caught: number;
   battle_attack_won: number;
   battle_attack_total: number;
   battle_defended_won: number;
   battle_training_won: number;
   battle_training_total: number;
   prestige_raised_total: number;
   prestige_dropped_total: number;
   pokemon_deployed: number;
   pokemon_caught_by_type: Array<number>;
   small_rattata_caught: number;
}
