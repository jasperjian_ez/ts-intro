let isDone: boolean = false;    // Boolen
let decimal: number = 6;        // Number
let color: string = "blue";     // String
let list: number[] = [1, 2, 3]; // Array
let now: Date = new Date();     // Date
let list2: Array<number> = [1, 2, 3];       // Array
let x: [string, number] = ["hello", 10];    // Tuple

// Enum
enum Color { Red, Green, Blue };
let c: Color = Color.Green;

// Any
let notSure: any = 4;
notSure = "maybe a string instead";
notSure = false; // okay, definitely a boolean