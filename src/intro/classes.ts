class Pokemon {
    name: string; // public by default
    public cp: number = 2000;
    private iv: number = 100;

    constructor(theName: string) { this.name = theName; }
    attack(damages: number = 0) {
        console.log(`${this.name} causes ${damages} damage(s).`);
    }
    transfer() {
        console.log(`${this.name} transfer to the professor. You get 1 candy.`);
    }
    appraise() {
        if (this.iv > 82) {
            console.log(`Your ${this.name} is wonderful.`);            
        }        
    }
}

class Dragonite extends Pokemon {
    constructor(name: string) { super(name); }
    attack(damages = 50) {
        console.log(`${this.name} uses Hyper Beam..`);
        super.attack(damages);

    }
}

class Magikarp extends Pokemon {
    constructor(name: string) { super(name); }
    attack() {
        console.log(`${this.name} uses Splash..`);
        super.attack(0);
    }
}

let lovelyDragon = new Dragonite("My Lovely Dragon");
let uselessFish: Pokemon = new Magikarp("Useless Fish");

lovelyDragon.attack(9999);
lovelyDragon.appraise();
uselessFish.attack();
uselessFish.transfer();