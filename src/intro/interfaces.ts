interface Person {
    name: string;
    age?: number;
}

function hello(person: Person) {
    console.log(`Hello ${person.name}, your age is ${person.age}.`);
}

hello({ name: 'Jack', age: 18 });

let rose: Person = {
    name: 'Rose'
};

hello(rose);