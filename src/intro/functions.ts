// Named function
function add(x: number, y: number): number {
    return x + y;
}

// Anonymous function
let myAdd = function (x: number, y: number): number {
    return x + y;
};

function buildName(firstName: string, lastName = "Smith") {
    return firstName + " " + lastName;
}

console.log(buildName('Bob'));