function identity<T>(arg: T): T {    
    return arg;
}

let output = identity<string>("myString");  // type of output will be 'string'
let output2 = identity(1);  // type of output will be 'number'