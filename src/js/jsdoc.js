let fullname = 'jack';
let age = 18;

/** @type {string} */
let unknownFullname;

/** @type {number} */
let unknownAge;

/**
 * Print hello world
 *
 * @param {string} world
 */
function hello(world) {
    console.log(`Hello ${world}`);
}

/**
 *
 *
 * @param {JQuery} elems
 */
function isEmpty(elems) {
    return !elems || !elems.length;
}

(
    /**
    * @param {Window} window
    * @param {JQueryStatic} $
    */
    function (window, $) {
    }
)(window, jQuery);

/**
 *
 *
 * @param {Player} player
 */
function isGymAvailable(player) {
    return player.level >= 5;
}