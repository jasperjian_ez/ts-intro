# 認識 TypesScript Demo

## Install

```bash
# 安裝 TypeScript typings
## typescript: TypeScript compiler
## typings: TypeScript 定義檔套件管理員
npm i -g typescript typings

# 安裝專案用到的套件
npm install

# 告訴 webpack ts-loader 用 global typescript 編譯
# 如果 typescript 不是用 -g 安裝這行可不用執行
npm link typescript

# 編譯並啟動開發伺服器
npm run start
```

前往 <http://127.0.0.1:9090/webpack-dev-server/> 預覽

## Editor

* [TypeScript Editor Support](https://github.com/Microsoft/TypeScript/wiki/TypeScript-Editor-Support)

## Resources

### Official

* [TypeScript 官方網站](https://www.typescriptlang.org)
* [TypeScript 官方部落格](https://blogs.msdn.microsoft.com/typescript)
* [TypeScript Github](https://github.com/Microsoft/TypeScript)

### 講英文

* [Awesome TypeScript](https://github.com/dzharii/awesome-typescript)

### 講中文

* [使用 TypeScript 開發大型應用程式](https://mva.microsoft.com/zh-tw/training-courses/-dev-developer-platform-and-tools--11254?l=yfuWsUKBB_1504984382)
* [TypeScript 中文教學手冊](https://github.com/kkbruce/TypeScript/blob/master/doc/zh-tw/Handbook.md)
* [TypeScript 簡介](http://www.winwu.cc/2016/07/typescript.html)
* [快速瞭解 TypeScript 是什麼東西](https://blogs.msdn.microsoft.com/ericsk/2012/10/01/typescript/)
* [TypeScript Handbook（中文版）](https://www.gitbook.com/book/zhongsp/typescript-handbook/details)
